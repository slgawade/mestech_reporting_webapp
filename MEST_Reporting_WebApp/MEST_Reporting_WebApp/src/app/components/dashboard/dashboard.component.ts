
import { Component, OnInit } from '@angular/core';
import { CoolLocalStorage } from 'angular2-cool-storage';
import { MasterService } from '../../services/master.service'
declare var $: any;

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    constructor(private cLS: CoolLocalStorage, private masterService: MasterService) {
        
    }

    ngOnInit() {
        
        $("#loaderDiv").css('display', 'none');
        this.masterService.rHeading = "";
       // this.cLS.setItem('reportHeading', '');  
    }

}
