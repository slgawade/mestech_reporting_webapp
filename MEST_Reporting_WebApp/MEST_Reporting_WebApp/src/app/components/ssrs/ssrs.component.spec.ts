import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SsrsComponent } from './ssrs.component';

describe('SsrsComponent', () => {
  let component: SsrsComponent;
  let fixture: ComponentFixture<SsrsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SsrsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SsrsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
