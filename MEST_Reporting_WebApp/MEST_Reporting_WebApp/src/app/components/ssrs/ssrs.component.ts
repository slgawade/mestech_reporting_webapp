import { Component, OnInit, OnDestroy } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { SsrsService } from '../../services/ssrs/ssrs.service';
import { MasterComponent } from '../master/master.component';
import { Daterangepicker } from 'ng2-daterangepicker';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { CoolLocalStorage } from 'angular2-cool-storage';
import * as moment from 'moment';

declare var $: any;
declare var _: any;
declare let require: any;

@Component({
    selector: 'app-ssrs',
    templateUrl: './ssrs.component.html',
    styleUrls: ['./ssrs.component.css'],
    providers: [SsrsService]
})
export class SsrsComponent implements OnInit {
    reportHeading: string;
    filterFlag: string;
    iFrameInteval: any;
    src: string;
    refId: string;
    category: string;
    filterDetails = [];
    constructor(
        private masterService: MasterService,
        private ssrsService: SsrsService,
        private masterComponent: MasterComponent,
        private daterangepickerOptions: DaterangepickerConfig,
        private cLS: CoolLocalStorage
    ) {

        this.reportHeading = this.cLS.getItem('reportHeading');
        this.filterFlag = this.cLS.getItem('filterFlag');
        this.masterService.filterFlag = this.cLS.getItem('filterFlag');
        //this.src = this.cLS.getItem('src');

         // Set Url to iFrame
        //this.setSourceToReport(this.src);

        this.category = this.cLS.getItem('category');
        this.refId = this.cLS.getItem('refId');
        
        //get SSRS fiters

        this.ssrsService.SSRS_Filters(this.refId, this.category);
    }

    ngAfterViewInit() {
        console.log('ssrc ngAfterViewInit');
        //this.setSourceToReport(this.src);
        $("#ssrs_Iframe").attr("src", this.src);
      //  this.loadAccordion();
    }

    ngOnInit() {
        
        console.log("ngOnInit called....!");

        this.startInterval();
    }
    oniFrameLoad(e) {
        let src = $(e.currentTarget).attr('src')

        if (src != undefined) {
            this.masterService.StopLoader();
        }

    }
    startInterval() {
        setInterval(() => {

            if (this.masterService.src_Flag == true) {
                this.src = this.cLS.getItem('src');
                this.setSourceToReport(this.src);
                this.masterService.src_Flag = false;

            }
        }, 10);
    }

    ngOnDestroy() {
        console.log('ngOnDestroy');
        clearInterval(this.iFrameInteval);
        //if (this.cLS.getItem('src') != null)
        //    this.cLS.removeItem('src')
    }

    setSourceToReport(src) {

        console.log("setSourceToReport : " + src);
        // $("#ssrs_Iframe").attr("src", src);
        console.log("data-value : " + $("#ssrs_Iframe").attr("data-value"))
        this.iFrameInteval = setInterval(() => {
            if ($("#ssrs_Iframe").attr("data-value") != undefined) {
                $("#ssrs_Iframe").attr("src", src);
                clearInterval(this.iFrameInteval);
                this.masterService.StopLoader();
                //setTimeout(() => {
                //    this.masterService.StopLoader();
                //}, 1500);

            }
            console.log("data-value : " + $("#ssrs_Iframe").attr("data-value"))
        }, 10);
    }

    DatePickerOptions() {
        this.daterangepickerOptions.settings = {
            locale: { format: 'YYYY-MM-DD' },
            alwaysShowCalendars: false,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 3 Months': [moment().subtract(4, 'month'), moment()],
                'Last 6 Months': [moment().subtract(6, 'month'), moment()],
                'Last 12 Months': [moment().subtract(12, 'month'), moment()],
            }
        };
    }

    public dateInputs: any = [
        {
            start: moment().subtract(29, 'days'),
            end: moment()
        }
    ];

    private selectedDate(value: any, dateInput: any) {
        dateInput.start = value.start;
        dateInput.end = value.end;
    }

    loadAccordion() {
        var Accordion = function (el, multiple) {
            this.el = el || {};
            // more then one submenu open?
            this.multiple = multiple || false;

            var dropdownlink = this.el.find('.dropdownlink');
            dropdownlink.on('click',
                { el: this.el, multiple: this.multiple },
                this.dropdown);
        };

        Accordion.prototype.dropdown = function (e) {
            var $el = e.data.el,
                $this = $(this),
                $next = $this.next();
            $next.slideToggle();
            $this.parent().toggleClass('open');

            if (!e.data.multiple) {
                //show only one menu at the same time
                $el.find('.submenuItems').not($next).slideUp().parent().removeClass('open');
            }
        }

        var accordion = new Accordion($('.accordion-menu'), false);
    }

    openNav() {
        var ft = document.getElementById("filterToggle").className;
        if (ft == "fa fa-angle-right") {
            document.getElementById("mySidenav").style.width = "300px";
            document.getElementById("main").style.marginLeft = "300px";

            document.getElementById("filterToggle").classList.add('fa-angle-left');

            document.getElementById("filterToggle").classList.remove('fa-angle-right');
        }
        else {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("main").style.marginLeft = "0";

            document.getElementById("filterToggle").classList.add('fa-angle-right');

            document.getElementById("filterToggle").classList.remove('fa-angle-left');
        }
    }


}
