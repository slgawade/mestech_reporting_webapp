import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MasterService } from '../../services/master.service';
import { SsrsService } from '../../services/ssrs/ssrs.service';
import { Daterangepicker } from 'ng2-daterangepicker';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { DaterangePickerComponent } from 'ng2-daterangepicker';

import { CoolLocalStorage } from 'angular2-cool-storage';
import * as moment from 'moment';
import { IMyDpOptions } from 'mydatepicker';
declare let $: any;

@Component({
    selector: 'master',
    templateUrl: './master.component.html',
    styleUrls: ['./master.component.css'],
    providers: [SsrsService],
    host: {
        '(document:click)': 'handleClick($event)',
    },
})
export class MasterComponent implements OnInit {

    FormData: any;
    dataSet = [];
    selectedFilterDS = [];
    filterPanel = [];
    filterControlDetails: any;
    Interval_Obs_FilterDetails: any;
    Interval_Obs_Dataset: any;
    hours: any;
    ind = 0;
    minutes: any;

    //filterToggle: boolean = false;


    constructor(
        private router: Router,
        private daterangepickerOptions: DaterangepickerConfig,
        private masterService: MasterService,
        private ssrsService: SsrsService,
        private cLS: CoolLocalStorage
    ) {

      //  this.cLS.setItem('src', '');

    }

    ngOnInit() {

        this.date_Timeout();

        this.loadHoursMinutes();
        this.masterService.get_Obs_FilterDetails().take(1).subscribe(data => {
            this.filterControlDetails = data;
        });
        this.masterService.get_Obs_Dataset().subscribe(data => {
            for (let i in data) {
                this.dataSet.push({
                    id: data[i].id,
                    name: data[i].name,
                    type: data[i].type
                });
            }
            console.log("OnInIt Dataset....");
            console.log(this.dataSet);
            this.masterService.StopLoader();
        });
        this.masterService.get_Obs_ToggleFilter().subscribe(data => {
            console.log('Filter : ' + data);
            this.filterControlDetails = [];
        });
    }

    date_Timeout() {
        setTimeout(() => {

            for (let i in this.filterControlDetails) {
                if (this.filterControlDetails[i].filterField == "DATE") {
                    this.datePicker(this.filterControlDetails[i].filterID);
                }
                else if (this.filterControlDetails[i].filterField == "DATETIME") {
                    this.dateTimePicker(this.filterControlDetails[i].filterID);
                }
                else if (this.filterControlDetails[i].filterField == "DATERANGE") {
                    this.dateRangePicker(this.filterControlDetails[i].filterID);
                }
                else if (this.filterControlDetails[i].filterField == "DATETIMERANGE") {
                    this.dateTimeRangePicker(this.filterControlDetails[i].filterID);
                }
            }
        }, 2000)
    }

    ngAfterViewChecked() {
        //console.log('sham');
    }

    ngDoCheck() {
        //console.log('ngDoCheck');
    }

    ngAfterContentChecked() {
        //console.log('ngAfterContentChecked');
        //console.log("height" + $('#fillter').height());

        let fheight = $('#fillter').height();
        fheight = fheight + 25;
        fheight = fheight + "px";
        if ($('#det').text() == '[Hide]') {
            $('.content-wrapper').css("height", "calc(100% - 61px)");
            $('#main').css("height", "calc(100% - " + fheight + ")");
        }
        else {
            $('.content-wrapper').css('height', 'calc(100% - 60px)');
            $('.content-wrapper').css('margin-top', '40px');
            $('#main').css("height", "98%");
            $('#mainSecondDiv iframe').css('margin-top', '0px');
        }

        $('#mainSecondDiv').css("height", "100%");
        $('#ssrs_Iframe').css("height", "98%");
    }

    ngAfterViewInit() {
        this.date_Timeout();
    }


    startIntervals(dataset) {
        console.log("start Interval :");
        console.log(dataset);
        this.dataSet.push(dataset);
    }

    //show fillter

    showfillter() {
        $('#fillter').toggle("slow");
        console.log($('#det').text());
        if ($('#det').text() == '[Hide]') {
            $('#mainSecondDiv iframe').css('margin-top', '40px');
            $('#det').text('[Show]');
        }
        else {
            $('#det').text('[Hide]');
            $('#mainSecondDiv iframe').css('margin-top', '0px');
        }
    }

    //end

    handleClick(event) {
        if ($(event.target).attr('data-name') != 'menu')
            $("#nav").slideUp('fast');
    }

    openDemo() {
        this.router.navigateByUrl("/Master/Demo");
    }

    openDDLlist(dataid) {
        //debugger;
        $("#accordionMenu li.ulli div.ullidiv ul.submenuItems").each(function (index) {
            let id = $(this).attr('id');
            if (id == dataid)
                $("#" + dataid).slideToggle();
            else
                $("#" + id).slideUp();
            // console.log(id);
        });
    }

    //openDDLlist()
    //{
    //    $("#Organisation1_list").slideToggle();
    //}

    itemClick(e, flag) {
       // this.masterService.StartLoader();
        debugger;
        let type = this.cLS.getItem('rtype');
        if (type == "SSRS")
            this.ssrs_Filter_Click(e, flag);
        else if (type == "FTVP")
            this.ftvp_Filter_Click(e, flag);

    }

    //start SSRS Filter Click
    ssrs_Filter_Click(e, flag) {
        console.log("ssrs_Filter_Click");

        let val_id: any;

        if ($(e.currentTarget).attr('data-multiselect') == "true") {
            val_id = this.checkBox(e);
        }
        else {
            val_id = this.radioButton(e);
            let filterID = $(e.currentTarget).attr('data-filterID');
            $("#" + filterID).slideToggle();
        }

        this.setParameterValue(e, val_id, flag);

        setTimeout(() => {
            this.insertFilter();
        }, 200);

        
        //this.insertFilter();

    }
    //end SSRS Filter Click

    //start FTVP Filter Click
    ftvp_Filter_Click(e, flag) {
        console.log("ftvp_Filter_Click");
        console.log(e);
    }
    //end FTVP Filter Click

    radioButton(e) {

        let filterID = $(e.currentTarget).attr('data-filterID');
        let index = $(e.currentTarget).attr('data-index');


        if ($(e.currentTarget).children('a').children('i').hasClass('unchecked')) {
            $("#" + filterID + " li.liai").each(function (index) {

                $(this).children('a').children('i').removeClass();
                $(this).children('a').children('i').addClass('iconColor unchecked');

                $(this).children('a').removeClass();
            });

            $(e.currentTarget).children('a').children('i').removeClass('unchecked');
            $(e.currentTarget).children('a').children('i').addClass('checked');
            $(e.currentTarget).children('a').addClass('checked');
        }
        //else {
        //    $("#" + filterID + " li.liai").each(function (index) {

        //        $(this).children('a').children('i').removeClass();
        //        $(this).children('a').children('i').addClass('iconColor unchecked')
        //    });

        //    $(e.currentTarget).children('a').children('i').removeClass('checked');
        //    $(e.currentTarget).children('a').children('i').addClass('unchecked');
        //}


        ////if ($("#" + filterID + " li.liai").length > 1) {

        ////    $("#" + filterID + " li.liai").each(function (index) {
        ////        $(this).children('a').children('i').removeClass();
        ////        $(this).children('a').children('i').addClass('iconColor unchecked')
        ////    });
        ////    $(e.currentTarget).children('a').children('i').removeClass('unchecked');
        ////    $(e.currentTarget).children('a').children('i').addClass('checked');
        ////}
        ////else {
        ////    $(e.currentTarget).children('a').children('i').removeClass('unchecked');
        ////    $(e.currentTarget).children('a').children('i').addClass('checked');
        ////}

        let ds_ids = [];

        $("#" + filterID + " li.liai").each(function (index) {
            if ($(this).children('a').children('i').hasClass('checked')) {
                ds_ids.push({
                    id: $(this).attr('data-valid'),
                    name: $(this).attr('data-valname'),
                    filter: $(this).attr('data-filter'),
                    reportParameter: $(this).attr('data-reportParameter')
                });
            }
        });

        let ids: string = '';
        for (let l in ds_ids) {
            ids += "'" + ds_ids[l].id + "',";
        }
        ids = ids.substring(0, ids.length - 1);

        console.log("this.selectedFilterDS : ");
        console.log(this.selectedFilterDS);

        return ids;
    }

    checkBox(e) {
        let filterID = $(e.currentTarget).attr('data-filterID');
        let index = $(e.currentTarget).attr('data-index');


        if ($(e.currentTarget).children('a').children('i').hasClass('fa-square-o')) {
            $(e.currentTarget).children('a').children('i').removeClass('fa-square-o');
            $(e.currentTarget).children('a').children('i').addClass('fa-check-square-o');
        }
        else {
            $(e.currentTarget).children('a').children('i').removeClass('fa-check-square-o');
            $(e.currentTarget).children('a').children('i').addClass('fa-square-o');
        }


        let ds_ids = [];

        $("#" + filterID + " li.liai").each(function (index) {
            if ($(this).children('a').children('i').hasClass('fa-check-square-o')) {
                ds_ids.push({
                    id: $(this).attr('data-valid'),
                    name: $(this).attr('data-valname'),
                    filter: $(this).attr('data-filter'),
                    reportParameter: $(this).attr('data-reportParameter')
                });
            }
        });

        let ids: string = '';
        for (let l in ds_ids) {
            ids += "'" + ds_ids[l].id + "',";
            // ids += ds_ids[l].id + ",";
        }
        ids = ids.substring(0, ids.length - 1);

        console.log("this.selectedFilterDS : ");
        console.log(this.selectedFilterDS);

        return ids;
    }

    insertFilter() {

        let ds = [];
        this.selectedFilterDS = [];
        $("#accordionMenu li.ulli div.ullidiv ul.submenuItems > div.smi_div >li.liai").each(function (e) {
            // debugger;
            if ($(this).children('a').children('i').hasClass('fa-check-square-o') || $(this).children('a').children('i').hasClass('checked')) {
                ds.push({
                    id: $(this).attr('data-valid'),
                    name: $(this).attr('data-valname'),
                    filterText: $(this).attr('data-filterText'),
                    filter: $(this).attr('data-filter'),
                    filterID: $(this).attr('data-filterID'),
                    reportParameter: $(this).attr('data-reportParameter'),
                    multiselect: $(this).attr('data-multiselect'),
                    storedProcedureName: $(this).attr('data-storedProcedureName'),
                    connectionString: $(this).attr('data-connectionString'),
                    dependent: $(this).attr('data-dependent'),
                    nextDep: $(this).attr('data-nextDep'),
                    storedProcedureParameter: $(this).attr('data-storedProcedureParameter'),
                    liId: $(this).attr('id')
                });
            }

        });

        for (let d in ds) {
            this.selectedFilterDS.push({
                id: ds[d].id,
                name: ds[d].name,
                filterText: ds[d].filterText,
                filter: ds[d].filter,
                filterID: ds[d].filterID,
                reportParameter: ds[d].reportParameter,
                multiselect: ds[d].multiselect,
                storedProcedureName: ds[d].storedProcedureName,
                connectionString: ds[d].connectionString,
                dependent: ds[d].dependent,
                nextDep: ds[d].nextDep,
                storedProcedureParameter: ds[d].storedProcedureParameter,
                liId: ds[d].liId
            });
        }

        this.filterPanel = [];
        let fil = [];
        for (let f1 in this.selectedFilterDS) {
            let dataset = [];

            if (fil.indexOf(this.selectedFilterDS[f1].filter) == -1) {
                fil.push(this.selectedFilterDS[f1].filter);

                for (let f2 in this.selectedFilterDS) {

                    if (this.selectedFilterDS[f1].filter == this.selectedFilterDS[f2].filter) {

                        dataset.push({
                            id: this.selectedFilterDS[f2].id,
                            name: this.selectedFilterDS[f2].name,
                            filterText: this.selectedFilterDS[f2].filterText,
                            filter: this.selectedFilterDS[f2].filter,
                            filterID: this.selectedFilterDS[f2].filterID,
                            reportParameter: this.selectedFilterDS[f2].reportParameter,
                            multiselect: this.selectedFilterDS[f2].multiselect,
                            storedProcedureName: this.selectedFilterDS[f2].storedProcedureName,
                            connectionString: this.selectedFilterDS[f2].connectionString,
                            dependent: this.selectedFilterDS[f2].dependent,
                            nextDep: this.selectedFilterDS[f2].nextDep,
                            storedProcedureParameter: this.selectedFilterDS[f2].storedProcedureParameter,
                            liId: this.selectedFilterDS[f2].liId
                        });

                    }

                }
                this.filterPanel.push({
                    id: this.selectedFilterDS[f1].filterText,
                    values: dataset
                });
            }
            
        }

        console.log("InserFilter()..selectedFilterDS : ");
        console.log(this.selectedFilterDS);
        console.log(this.filterPanel);
       
    }

    removeFilter(e) {

        let liId = $(e.currentTarget).attr('data-liId');
        let multiselect = $(e.currentTarget).attr('data-multiselect');

        if (multiselect == 'true') {
            $("#" + liId).children('a').children('i').removeClass('fa-check-square-o');
            $("#" + liId).children('a').children('i').addClass('fa-square-o');
        }
        else {
            $("#" + liId).children('a').children('i').removeClass('checked');
            $("#" + liId).children('a').children('i').addClass('unchecked');
        }

        this.itemClick(e, true);
    }

    removeAllFilters() {
        this.selectedFilterDS = [];
        $("#accordionMenu li.ulli div.ullidiv > ul.submenuItems > div >li.liai").each(function (e) {
            let multiselect = $(this).attr('data-multiselect');
            if (multiselect == 'true') {

                $(this).children('a').children('i').removeClass();
                $(this).children('a').children('i').addClass('iconColor fa fa-square-o');
            }
            else {
                $(this).children('a').children('i').removeClass();
                $(this).children('a').children('i').addClass('iconColor unchecked');
            }
        });
        //debugger;
    }

    setParameterValue(e, v_id, flag) {

        let storedProcedureName = $(e.currentTarget).attr('data-storedProcedureName');
        let connectionString = $(e.currentTarget).attr('data-connectionString');
        let nextDep = $(e.currentTarget).attr('data-nextDep');
        let index = $(e.currentTarget).attr('data-index');
        let param = $(e.currentTarget).attr('data-storedProcedureParameter');
        let filterId = $(e.currentTarget).attr('data-filter');

        let p1 = '', p2 = '', p3 = '', p4 = '', p5 = '', p6 = '', p7 = '', p8 = '',
            p9 = '', p10 = '', p11 = '', p12 = '', p13 = '', p14 = '', p15 = '';
        if ("parameter1" == param)
            p1 = v_id;
        if ("parameter2" == param)
            p2 = v_id;
        if ("parameter3" == param)
            p3 = v_id;
        if ("parameter4" == param)
            p4 = v_id;
        if ("parameter5" == param)
            p5 = v_id;
        if ("parameter6" == param)
            p6 = v_id;
        if ("parameter7" == param)
            p7 = v_id;
        if ("parameter8" == param)
            p8 = v_id;
        if ("parameter9" == param)
            p9 = v_id;
        if ("parameter10" == param)
            p10 = v_id;
        if ("parameter11" == param)
            p11 = v_id;
        if ("parameter12" == param)
            p12 = v_id;
        if ("parameter13" == param)
            p13 = v_id;
        if ("parameter14" == param)
            p14 = v_id;
        if ("parameter15" == param)
            p15 = v_id;

        this.ssrsService.getDatasetForFilter(
            flag, nextDep, storedProcedureName, connectionString, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15
        ).subscribe(res => {
            console.log("dataset : ");
            console.log(res);
            //remove existing array
            let temp = this.dataSet.slice(0);
            let FCD = this.filterControlDetails;
            let len = this.filterControlDetails.length;
            for (let f in FCD) {

                for (let t = 0; t < temp.length; t++) {
                    if (temp[t].type == nextDep) {
                        temp.splice(t, 1);
                        t--;
                    }
                }
                for (let ff in FCD) {
                    if (nextDep == FCD[ff].filterID) {
                        nextDep = FCD[ff].nextDep;
                        break;
                    }
                }
            }

            this.dataSet = temp;
            if (false == false) {
                //adding new array  sample
                for (let l in res) {
                    this.dataSet.push({
                        id: res[l].id,
                        name: res[l].name,
                        type: res[l].type
                    });
                }
            }

            console.log("ssrs_Filter_Click : ");
            console.log(this.dataSet);
            console.log("filtered Data : ");
            console.log(this.selectedFilterDS);
            //this.masterService.StopLoader();
        });
    }

    sendParameterToReport() {
        this.masterService.StartLoader();
        let data: any;
        let reportId = this.cLS.getItem('reportId');
        let targetPageUrl = this.cLS.getItem('targetPageUrl');
        let rtype = this.cLS.getItem('rtype');
        let externalParam = "";
        let temp = [];
        for (let s in this.selectedFilterDS) {

            let rptParameter = this.selectedFilterDS[s].reportParameter;
            let ids = "";


            if (temp.indexOf(rptParameter) == -1) {
                temp.push(rptParameter);
                for (let i in this.selectedFilterDS) {
                    if (rptParameter == this.selectedFilterDS[i].reportParameter)
                        ids += this.selectedFilterDS[i].name + "|";
                }
                externalParam += "&" + rptParameter + "=" + ids.substring(0, ids.length - 1);
            }

        }

        for (let f in this.filterControlDetails) {
            if (this.filterControlDetails[f].filterField == "TEXTBOX") {
                let textbox = $("#filter_" + this.filterControlDetails[f].filterID).val();
                externalParam += "&" + this.filterControlDetails[f].reportParameter + "=" + textbox;
                console.log('TextBox : ' + this.filterControlDetails[f].filterID + ' : ');
                console.log(textbox);
            }
            else if (this.filterControlDetails[f].filterField == "DATE") {
                let date = $("#" + this.filterControlDetails[f].filterID + "_DATE").val();
                externalParam += "&" + this.filterControlDetails[f].reportParameter + "=" + date;
                console.log('date : ');
                console.log(date);
            }
            else if (this.filterControlDetails[f].filterField == "TIME") {
                let hh = $("#" + this.filterControlDetails[f].filterID + "_hh").val();
                let mm = $("#" + this.filterControlDetails[f].filterID + "_mm").val();
                let aa = $("#" + this.filterControlDetails[f].filterID + "_aa").val();

                externalParam += "&" + this.filterControlDetails[f].reportParameter + "=" + hh + ":" + mm + ":" + aa;

                console.log('hh + ":" + mm + ":" + aa');
                console.log(hh + ":" + mm + ":" + aa);
            }
            else if (this.filterControlDetails[f].filterField == "DATETIME") {
                let dtime = $("#" + this.filterControlDetails[f].filterID + "_DATETIME").val();
                externalParam += "&" + this.filterControlDetails[f].reportParameter + "=" + dtime;
                console.log(dtime);
            }
            else if (this.filterControlDetails[f].filterField == "DATERANGE") {
                let daterange_start = $("#" + this.filterControlDetails[f].filterID + "_From_DATERANGE").val();
                let daterange_end = $("#" + this.filterControlDetails[f].filterID + "_To_DATERANGE").val();

                externalParam += "&" + (this.filterControlDetails[f].reportParameter).split(',')[0] + "=" + daterange_start;
                externalParam += "&" + (this.filterControlDetails[f].reportParameter).split(',')[1] + "=" + daterange_end;

                console.log('daterange_start and daterange_end');
                console.log(daterange_start + ' and ' + daterange_end);
            }
            else if (this.filterControlDetails[f].filterField == "DATETIMERANGE") {
                let datetimerange_start = $("#" + this.filterControlDetails[f].filterID + "_From_DATETIMERANGE").val();
                let datetimerange_end = $("#" + this.filterControlDetails[f].filterID + "_To_DATETIMERANGE").val();

                externalParam += "&" + (this.filterControlDetails[f].reportParameter).split(',')[0] + "=" + datetimerange_start;
                externalParam += "&" + (this.filterControlDetails[f].reportParameter).split(',')[1] + "=" + datetimerange_end;

                console.log('datetimerange_start and datetimerange_end');
                console.log(datetimerange_start + ' and ' + datetimerange_end);
            }
        }

        console.log("externalParam :-- " + externalParam);
        this.masterService.getServerDetails().take(1).subscribe(response => {

            if (response.data.length > 0) {

                try {
                    let details = response.data;
                    let src = "";

                    for (let d in details) {
                        if (details[d].type == rtype)
                            src = details[d].http + details[d].hostName + details[d].portal + reportId;
                    }
                    src += "&rc:Zoom=page+width" + externalParam;
                    console.log("sendParameterToReport : " + src);
                    this.cLS.setItem('src', src);
                    this.masterService.src_Flag = true;
                    this.router.navigateByUrl(targetPageUrl);

                    // this.masterService.updateData(src);
                    //if (type == "SSRS")
                    //    this.get_SSRS_ReportFilterDetails(refId, category);

                } catch (e) {
                    console.log("========Error========" + e);
                }
            }
        });
    }

    //start filter value Search
    searchFilterValue(e) {
        console.log($(e.currentTarget).val());
        let id = $(e.currentTarget).attr('data-filterID');
        let ind, input, filter, ul, li, a, i;
        filter = $(e.currentTarget).val().toUpperCase();
        ul = document.getElementById($(e.currentTarget).attr('data-filterID'));
        li = ul.getElementsByTagName("li");
        for (i = 1; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            if (a.text.toUpperCase().indexOf(filter) > -1) {
                $(li[i]).css('display', 'block');
            } else {
                $(li[i]).css('display', 'none')

            }
        }
    }
    //end filter value Search

    //start time picker
    loadHoursMinutes() {
        this.hours = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        this.minutes = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10',
            '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
            '21', '22', '23', '24', '25', '26', '27', '28', '29', '30',
            '31', '32', '33', '34', '35', '36', '37', '38', '39', '40',
            '41', '42', '43', '44', '45', '46', '47', '48', '49', '50',
            '51', '52', '53', '54', '55', '56', '57', '58', '59', '60',
        ];
    }

    timeValidation(id) {

        let hh = $('#' + id + '_hh').val();
        let mm = $('#' + id + '_mm').val();

        if (hh == 'HH') {
            $('#' + id + '_formControl').addClass('inputboxError');
            $('#' + id + '_inputAddon').addClass('boxbtnError');
            // $('#dateErrorLabelTime').css('display', 'block');
            $('#' + id + '_formControl').removeClass('inputboxSuccess');
            $('#' + id + '_inputAddon').removeClass('boxbtnSuccess');
        }
        else if (mm == 'MM') {
            $('#' + id + '_formControl').addClass('inputboxError');
            $('#' + id + '_inputAddon').addClass('boxbtnError');
            //$('#dateErrorLabelTime').css('display', 'block');
            $('#' + id + '_formControl').removeClass('inputboxSuccess');
            $('#' + id + '_inputAddon').removeClass('boxbtnSuccess');
        }
        else if (hh == '00' && mm == '00') {
            $('#' + id + '_formControl').addClass('inputboxError');
            $('#' + id + '_inputAddon').addClass('boxbtnError');
            //$('#dateErrorLabelTime').css('display', 'block');
            $('#' + id + '_formControl').removeClass('inputboxSuccess');
            $('#' + id + '_inputAddon').removeClass('boxbtnSuccess');
        }
        else {
            $('#' + id + '_formControl').removeClass('inputboxError');
            $('#' + id + '_inputAddon').removeClass('boxbtnError');
            $('#' + id + '_formControl').addClass('inputboxSuccess');
            $('#' + id + '_inputAddon').addClass('boxbtnSuccess');
            //$('#dateErrorLabelTime').css('display', 'none'); 
        }



    }
    //end time picker

    //start datetime picker
    dateTimePicker(date) {
        //debugger;
        $('#' + date + '_dtp').datetimepicker({
            date: new Date()
        });

        $('#' + date).formValidation({
            framework: 'bootstrap',
            fields: {
                [date]: {
                    validators: {
                        notEmpty: {
                            message: 'The date is required'
                        },
                        date: {
                            format: 'MM/DD/YYYY h:m A',
                            message: 'The value is not a valid date'
                        }
                    }
                }
            }
        });

        $('#' + date + '_dtp').on('dp.change dp.show', function (e) {
            $('#' + date).formValidation('revalidateField', date);
        });
    }
    //end datetime picker

    //start date picker
    datePicker(date) {

        $('#' + date + '_dtp').datetimepicker({
            date: new Date(),
            format: 'MM/DD/YYYY'
        });

        $('#' + date).formValidation({
            framework: 'bootstrap',
            fields: {
                [date]: {
                    validators: {
                        notEmpty: {
                            message: 'The date is required'
                        },
                        date: {
                            format: 'MM/DD/YYYY',
                            message: 'The value is not a valid date'
                        }
                    }
                }
            }
        });

        $('#' + date + '_dtp').on('dp.change dp.show', function (e) {
            $('#' + date).formValidation('revalidateField', date);
        });
    }
    //end date picker

    //start date range picker
    dateRangePicker(date) {
        let fromDate = "";
        let From = date + '_From';
        let To = date + '_To';
        // From Date.....
        $('#' + date + '_From_dt').datetimepicker({
            format: 'MM/DD/YYYY'
        });

        $('#' + date + '_From').formValidation({
            framework: 'bootstrap',
            fields: {
                [From]: {
                    validators: {
                        notEmpty: {
                            message: 'The date is required'
                        },
                        date: {
                            format: 'MM/DD/YYYY',
                            message: 'The value is not a valid date'
                        }
                    }
                }
            }
        });

        $('#' + date + '_From_dt').on('dp.change dp.show', function (e) {
            fromDate = $('#' + date + '_From_dt').find("input").val();
            $('#' + date + '_To_dtp').data('DateTimePicker').destroy();
            let fDate = new Date(fromDate);
            let newdate = new Date(fromDate);
            fDate.setDate(fDate.getDate());
            newdate.setDate(newdate.getDate() - 1);

            $('#' + date + '_To_dtp').datetimepicker({
                format: 'MM/DD/YYYY',
                date: fDate,
                minDate: newdate,
                disabledDates: [newdate]
            });
            $('#' + date + '_To').formValidation('revalidateField', date + '_To');
            $('#' + date + '_From').formValidation('revalidateField', date + '_From');
        });

        // To Date.....

        $('#' + date + '_To_dtp').datetimepicker({
            format: 'MM/DD/YYYY'
        });

        $('#' + date + '_To').formValidation({
            framework: 'bootstrap',
            fields: {
                [To]: {
                    validators: {
                        notEmpty: {
                            message: 'The date is required'
                        },
                        date: {
                            format: 'MM/DD/YYYY',
                            message: 'The value is not a valid date'
                        }
                    }
                }
            }
        });

        $('#' + date + '_To_dtp').on('dp.change dp.show', function (e) {
            $('#' + date + '_To').formValidation('revalidateField', date + '_To');
        });
    }
    //end date range picker

    //start datetime range picker
    dateTimeRangePicker(date) {
        let fromDate = "";
        let From = date + '_From';
        let To = date + '_To';
        // From Date.....
        $('#' + date + '_From_dt').datetimepicker();

        $('#' + date + '_From').formValidation({
            framework: 'bootstrap',
            fields: {
                [From]: {
                    validators: {
                        notEmpty: {
                            message: 'The date is required'
                        },
                        date: {
                            format: 'MM/DD/YYYY h:m a',
                            message: 'The value is not a valid date'
                        }
                    }
                }
            }
        });

        $('#' + date + '_From_dt').on('dp.change dp.show', function (e) {
            fromDate = $('#' + date + '_From_dt').find("input").val();
            $('#' + date + '_To_dtp').data('DateTimePicker').destroy();

            let fDate = new Date(fromDate);
            if (fDate.toString() != "Invalid Date") {
                let newdate = new Date(fromDate);
                fDate.setDate(fDate.getDate());
                newdate.setDate(newdate.getDate() - 1);

                $('#' + date + '_To_dtp').datetimepicker({
                    format: 'MM/DD/YYYY h:m A',
                    date: fDate,
                    minDate: newdate,
                    disabledDates: [newdate]
                });
            }
            $('#' + date + '_To').formValidation('revalidateField', date + '_To');
            $('#' + date + '_From').formValidation('revalidateField', date + '_From');
        });

        // To Date.....

        $('#' + date + '_To_dtp').datetimepicker();

        $('#' + date + '_To').formValidation({
            framework: 'bootstrap',
            fields: {
                [To]: {
                    validators: {
                        notEmpty: {
                            message: 'The date is required'
                        },
                        date: {
                            format: 'MM/DD/YYYY h:m a',
                            message: 'The value is not a valid date'
                        }
                    }
                }
            }
        });

        $('#' + date + '_To_dtp').on('dp.change dp.show', function (e) {
            $('#' + date + '_To').formValidation('revalidateField', date + '_To');
        });
    }
    //end datetime range picker

    PickerChange(e, todateid) {
        let fromId = $(e.currentTarget).attr("data-from");
        let FromDate = $("#" + fromId).val();
        let ToDate = $(e.currentTarget).val();

        FromDate = new Date(FromDate);
        ToDate = new Date(ToDate);

        if (FromDate.getTime() >= ToDate.getTime()) {
            console.log("bhau");
            //  $(e.currentTarget).val($("#" + fromId).val());
        }
        $('#' + todateid + '_To').formValidation('revalidateField', todateid + '_To');
    }
}


// Header Components ......
@Component({
    selector: 'mstheader',
    templateUrl: `./header.html`,
    styleUrls: ['./master.component.css']
})


export class HeaderComponent {
    MenuList: any;
    username: string;
    rHeading: string = "";
    constructor(
        private masterService: MasterService,
        private ssrsService: SsrsService,
        private router: Router,
        private cLS: CoolLocalStorage
    ) {

        // this.reuseRouter();
        
        this.masterService.rHeading = this.cLS.getItem('reportHeading');
    }

    ngOnInit() {

        this.getMenuDetails();
        this.masterService.get_Obs_ToggleFilter().subscribe(data => {
            console.log('Filter : ' + data);
            this.rHeading = "";
        });
    }

    ngAfterViewChecked() {

    }
    ngAfterContentInit() {
        // this.username = this.cLS.getItem('username');

        // this.rHeading = this.cLS.getItem('reportHeading');
    }
    ngAfterViewInit() {
        // this.username = this.cLS.getItem('username');

    }

    getMenuDetails() {
        let data: any, d: any; this.MenuList = [];
        this.masterService.getMenuList().take(1).subscribe(response => {


            if (response.data.length > 0) {

                try {
                    this.MenuList = response.data;
                } catch (e) {
                    console.log("error in getMenuDetails() : " + e); // error in the above string (in this case, yes)!
                }
            }
            console.log("this.MenuList :-");
            console.log(this.MenuList);

        });
    }

    menuClick(event) {

        this.masterService.StartLoader();
        let data: any;
        let category = $(event.currentTarget).attr('data-category');
        let refId = $(event.currentTarget).attr('data-id');
        let reportId = $(event.currentTarget).attr('data-reportId');
        let targetPageUrl = $(event.currentTarget).attr('data-targetPageUrl');
        let reportHeading = $(event.currentTarget).attr('data-reportHeading');
        this.masterService.rHeading = reportHeading;
        let filterFlag = $(event.currentTarget).attr('data-filterFlag');
        let rtype = $(event.currentTarget).attr('data-type');

        //storing value in session
        this.cLS.setItem('category', category);
        this.cLS.setItem('refId', refId);
        this.cLS.setItem('filterFlag', filterFlag);
        this.cLS.setItem('reportHeading', reportHeading);
        

        this.cLS.setItem('rtype', rtype);

        this.cLS.setItem('targetPageUrl', targetPageUrl);
        this.cLS.setItem('reportId', reportId);


        this.masterService.getServerDetails().take(1).subscribe(response => {

            if (response.data.length > 0) {

                try {
                    let details = response.data;
                    let src = "";
                    for (let d in details) {
                        if (details[d].type == rtype)
                            src = details[d].http + details[d].hostName + details[d].portal + reportId;
                    }

                    console.log("menuClick : " + src);
                    this.cLS.setItem('src', src);
                   // this.masterService.src_Flag = true;

                    this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
                        this.router.navigate([targetPageUrl]));
                   
                  
                    // this.router.navigateByUrl(targetPageUrl);
                    // this.masterService.update_Dataset(src);

                } catch (e) {
                    console.log("========Error========" + e);
                }
            }
        });

    }

    //toggleMenu() {
    //    $("#nav").slideToggle('fast');
    //}

    toggleMenu() {
        if ($("body").hasClass("sidebar-collapse")) {
            $("body").removeClass("sidebar-collapse");
        }
        else {
            $("body").addClass("sidebar-collapse");
        }

    }

    reuseRouter() {
        // override the route reuse strategy
        this.router.routeReuseStrategy.shouldReuseRoute = function () {
            return false;
        }

        this.router.events.subscribe((evt) => {
            this.router.navigated = false;
            this.router.navigate([this.router.url]);
        });
    }
    dashboard() {
        this.masterService.update_Obs_ToggleFilter(false);
        this.rHeading = "";
        this.cLS.setItem('reportHeading', '');
        this.router.navigateByUrl('Reports/Dashboard');
    }
    login() {
        this.masterService.update_Obs_ToggleFilter(false);
        this.rHeading = "";
        this.cLS.setItem('reportHeading', '');
        this.router.navigateByUrl('Login');
    }
}


// Footer Component .....
@Component({
    selector: 'mstfooter',
    templateUrl: `./footer.html`,
    styleUrls: ['./master.component.css']
})
export class FooterComponent {
    constructor() {

    }
    ngOnInit() {

    }
    ngAfterViewChecked() {

    }

}
