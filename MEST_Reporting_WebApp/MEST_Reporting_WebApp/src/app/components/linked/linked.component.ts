import { Component, OnInit, OnDestroy } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { SsrsService } from '../../services/ssrs/ssrs.service';
import { MasterComponent } from '../master/master.component';
import { CoolLocalStorage } from 'angular2-cool-storage';
import * as moment from 'moment';

declare var $: any;
declare var _: any;
declare let require: any;
@Component({
    selector: 'app-linked',
    templateUrl: './linked.component.html',
    styleUrls: ['./linked.component.css']
})
export class LinkedComponent implements OnInit {
    reportHeading: string;
    filterFlag: string;
    src: string;
    iFrameInteval: any;
    refId: string;
    category: string;
    filterDetails = [];
    constructor(
        private masterService: MasterService,
        private ssrsService: SsrsService,
        private masterComponent: MasterComponent,
        private cLS: CoolLocalStorage
    ) {

        this.reportHeading = this.cLS.getItem('reportHeading');
        this.filterFlag = this.cLS.getItem('filterFlag');
        this.masterService.filterFlag = this.cLS.getItem('filterFlag');

        this.category = this.cLS.getItem('category');
        this.refId = this.cLS.getItem('refId');

       // this.ssrsService.SSRS_Filters(this.refId, this.category);
    }

    ngAfterViewInit() {
        console.log('LINKED ngAfterViewInit');
        $("#linked_Iframe").attr("src", this.src);
    }

    ngOnInit() {

        console.log("ngOnInit called....!");

        this.startInterval();
    }
    oniFrameLoad(e) {
        let src = $(e.currentTarget).attr('src')

        if (src != undefined) {
            this.masterService.StopLoader();
        }

    }
    startInterval() {
        setTimeout(() => {
            
                this.src = this.cLS.getItem('src');
                this.setSourceToReport(this.src);
                this.masterService.src_Flag = false;
           
        }, 200);
    }

    ngOnDestroy() {
        console.log('ngOnDestroy');
        clearInterval(this.iFrameInteval);
    }

    setSourceToReport(src) {

        console.log("setSourceToReport : " + src);

        console.log("data-value : " + $("#linked_Iframe").attr("data-value"))
        this.iFrameInteval = setInterval(() => {

            if ($("#linked_Iframe").attr("data-value") != undefined) {
                $("#linked_Iframe").attr("src", src);
                clearInterval(this.iFrameInteval);
                this.masterService.StopLoader();

            }
            console.log("data-value : " + $("#linked_Iframe").attr("data-value"))
        }, 10);

    }




}
