import { Component, OnInit, OnDestroy } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { AmChartsService } from "amcharts3-angular2";
declare var $: any;
declare var _: any;
declare let require: any;
@Component({
    selector: 'app-test',
    templateUrl: './test.component.html',
    styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit, OnDestroy {
    private chart: any;
    constructor(private masterService: MasterService) {
        
    }

    childMethod() {
        console.log("Child Called..");
    }

    ngOnInit() {
        //this.chart = this.AmCharts.makeChart("chartdiv", {
        //    "type": "pie",
        //    "theme": "light",
        //    "dataProvider": [{
        //        "country": "Lithuania",
        //        "value": 260
        //    }, {
        //        "country": "Ireland",
        //        "value": 201
        //    }, {
        //        "country": "Germany",
        //        "value": 65
        //    }, {
        //        "country": "Australia",
        //        "value": 39
        //    }, {
        //        "country": "UK",
        //        "value": 19
        //    }, {
        //        "country": "Latvia",
        //        "value": 10
        //    }],
        //    "valueField": "value",
        //    "titleField": "country",
        //    "outlineAlpha": 0.4,
        //    "depth3D": 15,
        //    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        //    "angle": 30,
        //    "export": {
        //        "enabled": true
        //    }
        //});
        //// this.chart.path = "/node_modules/amcharts3/amcharts/";
    }
    ngOnDestroy() {
      //  this.AmCharts.destroyChart(this.chart);
    }

}
