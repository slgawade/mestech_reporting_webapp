import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FtvpComponent } from './ftvp.component';

describe('FtvpComponent', () => {
  let component: FtvpComponent;
  let fixture: ComponentFixture<FtvpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FtvpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FtvpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
