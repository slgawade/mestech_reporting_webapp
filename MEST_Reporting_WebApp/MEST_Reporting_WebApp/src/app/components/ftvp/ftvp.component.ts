import { Component, OnInit, OnDestroy } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { MasterComponent } from '../master/master.component';
import { Daterangepicker } from 'ng2-daterangepicker';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { CoolLocalStorage } from 'angular2-cool-storage';
import * as moment from 'moment';

declare var $: any;
declare var _: any;
declare let require: any;

@Component({
  selector: 'app-ftvp',
  templateUrl: './ftvp.component.html',
  styleUrls: ['./ftvp.component.css']
})
export class FtvpComponent implements OnInit {

    reportHeading: string;
    filterFlag: string;
    src: string;
    constructor(
        private masterService: MasterService,
        private masterComponent: MasterComponent,
        private daterangepickerOptions: DaterangepickerConfig,
        private cLS: CoolLocalStorage
    ) {
        this.reportHeading = this.cLS.getItem('reportHeading');
        this.filterFlag = this.cLS.getItem('filterFlag');
        this.src = this.cLS.getItem('src');

        this.setSourceToReport(this.src);
    }

    ngAfterViewInit() {
        this.loadAccordion();
    }
    ngAfterContentChecked() {
        $("#bottomPane").css('display', 'none');
    }
    ngOnInit() {
        console.log("ngOnInit called....!");

    }
    ngOnDestroy() {
        console.log('ngOnDestroy');
    }
    oniFrameLoad(e) {

        let src = $(e.currentTarget).attr('src')

        if (src != undefined) {
            this.masterService.StopLoader();
        }

    }
    setSourceToReport(src) {

        console.log("setSourceToReport : " + src);
        console.log("data-value : " + $("#ftvp_Iframe").attr("data-value"))
        let iFrameInteval = setInterval(() => {
            if ($("#ftvp_Iframe").attr("data-value") != undefined) {
                $("#ftvp_Iframe").attr("src", src);
                clearInterval(iFrameInteval);
                //setTimeout(() => {
                //    this.masterService.StopLoader();
                //}, 1500);
            }
            console.log("data-value : " + $("#ftvp_Iframe").attr("data-value"))
        }, 10);
    }

    DatePickerOptions() {
        this.daterangepickerOptions.settings = {
            locale: { format: 'YYYY-MM-DD' },
            alwaysShowCalendars: false,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 3 Months': [moment().subtract(4, 'month'), moment()],
                'Last 6 Months': [moment().subtract(6, 'month'), moment()],
                'Last 12 Months': [moment().subtract(12, 'month'), moment()],
            }
        };
    }

    public dateInputs: any = [
        {
            start: moment().subtract(29, 'days'),
            end: moment()
        }
    ];

    private selectedDate(value: any, dateInput: any) {
        dateInput.start = value.start;
        dateInput.end = value.end;
    }

    loadAccordion() {
        var Accordion = function (el, multiple) {
            this.el = el || {};
            // more then one submenu open?
            this.multiple = multiple || false;

            var dropdownlink = this.el.find('.dropdownlink');
            dropdownlink.on('click',
                { el: this.el, multiple: this.multiple },
                this.dropdown);
        };

        Accordion.prototype.dropdown = function (e) {
            //console.log('filter');
            var $el = e.data.el,
                $this = $(this),
                //this is the ul.submenuItems
                $next = $this.next();
            //debugger;
            $next.slideToggle();
            $this.parent().toggleClass('open');

            if (!e.data.multiple) {
                //show only one menu at the same time
                $el.find('.submenuItems').not($next).slideUp().parent().removeClass('open');
            }
        }

        var accordion = new Accordion($('.accordion-menu'), false);
    }

    openNav() {
        var ft = document.getElementById("filterToggle").className;
        if (ft == "fa fa-angle-right") {
            document.getElementById("mySidenav").style.width = "325px";
            document.getElementById("main").style.marginLeft = "325px";

            document.getElementById("filterToggle").classList.add('fa-angle-left');

            document.getElementById("filterToggle").classList.remove('fa-angle-right');
        }
        else {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("main").style.marginLeft = "0";

            document.getElementById("filterToggle").classList.add('fa-angle-right');

            document.getElementById("filterToggle").classList.remove('fa-angle-left');
        }
    }

}
