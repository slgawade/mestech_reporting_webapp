import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { routing } from './app.routing';
import { MasterService } from './services/master.service';
import { ReactiveFormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';

//daterange picker
import { Daterangepicker } from 'ng2-daterangepicker';
//cool storage
import { CoolStorageModule } from 'angular2-cool-storage';
//amCharts
import { AmChartsModule } from "amcharts3-angular2";

//components
import { AppComponent, GlobalService } from './app.component';
import { MasterComponent, HeaderComponent, FooterComponent  } from './components/master/master.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SsrsComponent } from './components/ssrs/ssrs.component';
import { FtvpComponent } from './components/ftvp/ftvp.component';
import { TestComponent } from './components/test/test.component';
import { LinkedComponent } from './components/linked/linked.component';


@NgModule({
  declarations: [
    AppComponent,
      MasterComponent,
      HeaderComponent,
      FooterComponent,
      LoginComponent,
      DashboardComponent,
      SsrsComponent,
      FtvpComponent,
      TestComponent,
      LinkedComponent 
  ],
  imports: [
      BrowserModule,
      FormsModule,
      HttpModule,
      routing,
      Daterangepicker,
      CoolStorageModule,
      ReactiveFormsModule,
      MyDatePickerModule,
      AmChartsModule
  ],
  providers: [MasterService, GlobalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
