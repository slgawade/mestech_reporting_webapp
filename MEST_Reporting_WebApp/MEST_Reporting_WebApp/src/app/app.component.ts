import { Component } from '@angular/core';

declare var $: any;
declare var _: any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'app';
    constructor() {
        $("#LoginloaderDiv").css('display', 'none');
        $("#loaderDiv").css('display', 'none');
    }
}

export class GlobalService {
    //Url: string = "http://localhost:52104/api/";
    Url: string = "http://192.168.14.9/ADIENT_Services/api/";
    //Url: string = "http://192.168.208.73/KukaServices/api/";
    //Url: string = "http://localhost/KukaServices/api/";

}