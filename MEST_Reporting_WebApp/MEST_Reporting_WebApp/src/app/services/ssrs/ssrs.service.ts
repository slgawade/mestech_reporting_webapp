import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import { MasterService } from '../master.service';
import { GlobalService } from '../../app.component';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Rx';

@Injectable()
export class SsrsService {
    api: string;
    constructor(private http: Http, private masterService: MasterService, private global: GlobalService) {
        this.api = global.Url + 'Filters/';
    }
    getFilterDetails() {
        console.log("getFilterDetails")
        return this.http.get("assets/config/SSRS/filter.json").map(response =>
            response.json());
    }

    //Get SSRS filters Details
    SSRS_Filters(refId, category) {
       
        let responseData: any, data: any;
        let filterControlDetails = [], dataSet = [];
        this.getFilterDetails().take(1).subscribe(response => {

            //for (let i in response) {
            //    if (i == "_body") {
            //        responseData = response[i];
            //    }
            //}
            if (response.data.length > 0) {

                try {
                    data = response.data;
                    for (let d in data) {
                        if (data[d].category == category) {

                            let category_values = data[d].values;

                            for (let cv in category_values) {

                                if (category_values[cv].refId == refId) {
                                    filterControlDetails = category_values[cv].values;
                                  
                                    for (let f in filterControlDetails) {
                                        if ((filterControlDetails[f].dependent) == true) {
                                           
                                            this.getDatasetForFilter(false,
                                                filterControlDetails[f].filterID,
                                                filterControlDetails[f].storedProcedureName,
                                                filterControlDetails[f].connectionString,
                                                '', '', '', '', '', '', '', '', '', '', '', '', '', '', '').subscribe(res => {
                                                    console.log("SSRS_Filters: dataset : ");
                                                    console.log(res);
                                                    this.masterService.dataSet_Flag = true;

                                                    this.masterService.update_Obs_Dataset(res);

                                                });
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                } catch (e) {
                    alert(e);
                }
            }
            if (filterControlDetails.length > 0) {
                this.masterService.filterDetails_Flag = true;
                this.masterService.update_Obs_FilterDetails(filterControlDetails);
                console.log("FilterDetails Data :-");
                console.log(filterControlDetails);
            }
            else
                console.log("No Filters for this Reports.....!");
        });
    }

    getDatasetForFilter(flag, mode, storedProcedureName, connectionString,
        p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15) {
        console.log('mode, storedProcedureName, connectionString,p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15');
        console.log(mode + ':' + storedProcedureName + ':' + connectionString + ':' + p1 + ':' + p2 + ':' + p3 + ':' + p4 + ':' + p5 + ':' + p6 + ':' + p7 + ':' + p8 + ':' + p9 + ':' + p10 + ':' + p11 + ':' + p12 + ':' + p13 + ':' + p14 + ':' + p15);
        let ParamData = new URLSearchParams();
        ParamData.append("commandText", storedProcedureName);
        ParamData.append("ConString", connectionString);
        ParamData.append("mode", mode);
        ParamData.append("parameter1", p1);
        ParamData.append("parameter2", p2);
        ParamData.append("parameter3", p3);
        ParamData.append("parameter4", p4);
        ParamData.append("parameter5", p5);
        ParamData.append("parameter6", p6);
        ParamData.append("parameter7", p7);
        ParamData.append("parameter8", p8);
        ParamData.append("parameter9", p9);
        ParamData.append("parameter10", p10);
        ParamData.append("parameter11", p11);
        ParamData.append("parameter12", p12);
        ParamData.append("parameter13", p13);
        ParamData.append("parameter14", p14);
        ParamData.append("parameter15", p15);

        return this.http.post(this.api, ParamData).map(response =>
            response.json());
    }
}
