import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Rx';

declare var $: any;
declare var _: any;

@Injectable()
export class MasterService {

    public filterToggle$: Subject<boolean> = new Subject<boolean>();
    private ObsFilter$ = new Subject();
    private dataset$ = new Subject();
    private ObsDataset$ = new Subject();
    src_Flag: boolean = false;
    filterDetails_Flag: boolean = false;
    filterMode: string = "";
    filterFlag: string = "";
    rHeading: string = "";
    dataSet_Flag: boolean = false;
    constructor(private http: Http) {

    }

    get_Obs_ToggleFilter() {
        console.log("master service get_Obs_ToggleFilter Called....");
        return this.filterToggle$;
    }
    update_Obs_ToggleFilter(data: boolean) {
        console.log("master service update_Obs_ToggleFilter Called....");
        this.filterToggle$.next(data);
    }

    get_Obs_FilterDetails() {
        console.log("master service get_Obs_FilterDetails Called....");
        return this.ObsFilter$;
    }
    update_Obs_FilterDetails(data: any) {
        console.log("master service update_Obs_FilterDetails Called....");
        this.ObsFilter$.next(data);
    }

    get_Obs_Dataset() {
        console.log("master service get_Obs_Dataset Called....");
        return this.ObsDataset$;
    }
    update_Obs_Dataset(data: any) {
        console.log("master service update_Obs_Dataset Called....");
        this.ObsDataset$.next(data);
    }
    get_Dataset() {
        console.log("master service get_Dataset Called....");
        return this.dataset$;
    }
    update_Dataset(data: any) {
        console.log("master service update_Dataset Called....");
        this.dataset$.next(data);
    }
   
    getMenuList() {
        console.log("getMenuList")
        return this.http.get("assets/config/Shared/reportsDetails.json").map(response =>
            response.json());
    }
    getServerDetails() {
        console.log("getServerDetails")
        return this.http.get("assets/config/Shared/serverDetails.json").map(response =>
            response.json());
    }
    StartLoader() {
        $("#loaderDiv").css('display', 'block');
    }
    StopLoader() {
        $("#loaderDiv").css('display', 'none');
    }
 
}
