﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { AppComponent } from './app.component';
import { MasterComponent, HeaderComponent, FooterComponent } from './components/master/master.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SsrsComponent } from './components/ssrs/ssrs.component';
import { FtvpComponent } from './components/ftvp/ftvp.component';
import { LinkedComponent } from './components/linked/linked.component';
import { TestComponent } from './components/test/test.component';

const appRoutes: Routes = [
    {
        path: '',
        component: LoginComponent
    },
    {
        path: 'Login',
        component: LoginComponent
    },
    {
        path: 'Reports',
        component: MasterComponent,
        children: [
            {
                path: '',
                component: DashboardComponent
            },
            {
                path: 'Dashboard',
                component: DashboardComponent
            },
            {
                path: 'test',
                component: TestComponent
            },
            {
                path: 'SSRS',
                component: SsrsComponent
            },
            {
                path: 'FTVP',
                component: FtvpComponent
            },
            {
                path: 'LINKED',
                component: LinkedComponent
            }
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes, { useHash: true });